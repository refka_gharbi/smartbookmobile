package smart.book;



import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class BookListAdapter extends ArrayAdapter {
    Activity context;
    Integer [] portrait;
    String [] acheter;


    public BookListAdapter(Activity context, Integer[] portrait, String[] acheter) {
        super(context,R.layout.element,acheter);
        this.context = context;
        this.portrait = portrait;
        this.acheter = acheter;

    }

    @NonNull

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        View view=layoutInflater.inflate(R.layout.element,null,true);
        ImageView img = view.findViewById(R.id.image);
        TextView t1 = view.findViewById(R.id.acheter);
        img.setImageResource(portrait[position]);

        return view;
    }
}