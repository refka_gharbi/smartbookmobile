package smart.book;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    GridView list;
    String [] acheter={"Sortir des émotions négative","La Boussole Des Emotions","CULTIVEZ L'ENERGIE POSITIVE","Tout est illuminé","L'Enfant Lumières","La Demoiselle","Je crée mon site avec WordPress","HTML 5 et CSS3","Guide critique de l'évolution","TU COMPRENDRAS TA DOULEUR","NOURRIR LA PLANETE"};
    Integer [] portrait={R.drawable.b1,R.drawable.b2,R.drawable.b3,R.drawable.b4,R.drawable.b5,R.drawable.b6,R.drawable.b7,R.drawable.b8,R.drawable.b9,R.drawable.b10,R.drawable.b11};
    String[] Word= {"acheter"};
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list=findViewById(R.id.l);
        BookListAdapter adapter=new BookListAdapter(this,portrait,acheter);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder a= new AlertDialog.Builder(MainActivity.this);
                a.setTitle("Systeme d'exploitation");
                a.setIcon(portrait[position]);
                a.setMessage(acheter[position]);
                a.setPositiveButton("ok",null);
                a.show();
            }
        });

    }
}